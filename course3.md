# andrew Ng Machine Learning Specialization course3 notes

## week1
K-means, anomaly detection

![image](/stat/image%20(308).png)
![image](/stat/image%20(309).png)
![image](/stat/image%20(310).png)
![image](/stat/image%20(311).png)
![image](/stat/image%20(312).png)
![image](/stat/image%20(313).png)
![image](/stat/image%20(314).png)
![image](/stat/image%20(315).png)
![image](/stat/image%20(316).png)
![image](/stat/image%20(317).png)
![image](/stat/image%20(318).png)
![image](/stat/image%20(319).png)
![image](/stat/image%20(320).png)
![image](/stat/image%20(321).png)
![image](/stat/image%20(322).png)
![image](/stat/image%20(323).png)
![image](/stat/image%20(324).png)
![image](/stat/image%20(325).png)
![image](/stat/image%20(326).png)
![image](/stat/image%20(327).png)
![image](/stat/image%20(328).png)
![image](/stat/image%20(329).png)
![image](/stat/image%20(330).png)
![image](/stat/image%20(331).png)
![image](/stat/image%20(332).png)
![image](/stat/image%20(333).png)
![image](/stat/image%20(334).png)


## week2
Recommended system

![image](/stat/image%20(273).png)
![image](/stat/image%20(274).png)
![image](/stat/image%20(275).png)
![image](/stat/image%20(276).png)
![image](/stat/image%20(277).png)
![image](/stat/image%20(278).png)
![image](/stat/image%20(279).png)
![image](/stat/image%20(280).png)
![image](/stat/image%20(281).png)
![image](/stat/image%20(282).png)
![image](/stat/image%20(283).png)
![image](/stat/image%20(284).png)
![image](/stat/image%20(285).png)
![image](/stat/image%20(286).png)
![image](/stat/image%20(287).png)
![image](/stat/image%20(288).png)
![image](/stat/image%20(289).png)
![image](/stat/image%20(290).png)
![image](/stat/image%20(291).png)
![image](/stat/image%20(292).png)
![image](/stat/image%20(293).png)
![image](/stat/image%20(294).png)
![image](/stat/image%20(295).png)
![image](/stat/image%20(296).png)
![image](/stat/image%20(297).png)
![image](/stat/image%20(298).png)
![image](/stat/image%20(299).png)
![image](/stat/image%20(300).png)
![image](/stat/image%20(301).png)
![image](/stat/image%20(302).png)
![image](/stat/image%20(303).png)
![image](/stat/image%20(304).png)
![image](/stat/image%20(305).png)
![image](/stat/image%20(306).png)
![image](/stat/image%20(307).png)


## week3
Reinforce learning

![image](/stat/image%20(245).png)
![image](/stat/image%20(246).png)
![image](/stat/image%20(247).png)
![image](/stat/image%20(248).png)
![image](/stat/image%20(249).png)
![image](/stat/image%20(250).png)
![image](/stat/image%20(251).png)
![image](/stat/image%20(252).png)
![image](/stat/image%20(253).png)
![image](/stat/image%20(254).png)
![image](/stat/image%20(255).png)
![image](/stat/image%20(256).png)
![image](/stat/image%20(257).png)
![image](/stat/image%20(258).png)
![image](/stat/image%20(259).png)
![image](/stat/image%20(260).png)
![image](/stat/image%20(261).png)
![image](/stat/image%20(262).png)
![image](/stat/image%20(263).png)
![image](/stat/image%20(264).png)
![image](/stat/image%20(265).png)
![image](/stat/image%20(266).png)
![image](/stat/image%20(267).png)
![image](/stat/image%20(268).png)
![image](/stat/image%20(269).png)
![image](/stat/image%20(270).png)
![image](/stat/image%20(271).png)
![image](/stat/image%20(272).png)

