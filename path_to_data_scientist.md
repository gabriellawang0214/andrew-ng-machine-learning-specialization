# Path to Data Scientist Plan

## *beginner* coursera andrew Ng: Machine Learning Specialization https://www.coursera.org/learn/machine-learning (3-months) 

### (Done) course1: Supervised Machine Learning: Regression and Classification (https://www.coursera.org/learn/machine-learning#syllabus) 3 week
> notes： https://gitlab.com/gabriellawang0214/andrew-ng-machine-learning-specialization/-/blob/main/course1.md

  - - [x] week1 12/27
  - - [x] week2 12/28
  - - [x] week3 12/29

### (Done) course2: Advanced Learning Algorithms (https://www.coursera.org/learn/advanced-learning-algorithms?specialization=machine-learning-introduction#syllabus) 4 week 
> notes: https://gitlab.com/gabriellawang0214/andrew-ng-machine-learning-specialization/-/blob/main/course2.md
> 
  - - [x] week1 1/2/2023
  - - [x] week2 1/3/2023
 - - [x] week3 1/5/2023
 - - [x] week4 1/6/2023

### (Done) course3: Unsupervised Learning, Recommenders, Reinforcement Learning (https://www.coursera.org/learn/unsupervised-learning-recommenders-reinforcement-learning?specialization=machine-learning-introduction#syllabus) 3 week

> notes: https://gitlab.com/gabriellawang0214/andrew-ng-machine-learning-specialization/-/blob/main/course3.md

- - [x] week1 1/8/2023
- - [x] week2 1/11/2023
- - [x] week3 1/12/2023

## *intermediate* coursera andrew Ng: Deep Learning Specialization: https://www.coursera.org/specializations/deep-learning#courses (5 months)

1. course1: Neural Networks and Deep Learning (https://www.coursera.org/learn/neural-networks-deep-learning?specialization=deep-learning#syllabus) 4 week
2. course2:Improving Deep Neural Networks: Hyperparameter Tuning, Regularization and Optimization (https://www.coursera.org/learn/deep-neural-network?specialization=deep-learning#syllabus) 3 week
3. course3: Structuring Machine Learning Projects (https://www.coursera.org/learn/machine-learning-projects?specialization=deep-learning#syllabus) 2 week
4. course4: Convolutional Neural Networks (https://www.coursera.org/learn/convolutional-neural-networks?specialization=deep-learning#syllabus) 4 week
5. course5: Sequence Models (https://www.coursera.org/learn/nlp-sequence-models?specialization=deep-learning#syllabus)4 week

ETA 1/31 Done

## *Edx: MIT Machine Learning with Python: from Linear Models to Deep Learning*

Starts Feb 2, 2023 - Ends May 17, 2023

https://www.edx.org/course/machine-learning-with-python-from-linear-models-to

## *AWS Certified Machine Learning – Specialty*
https://aws.amazon.com/certification/certified-machine-learning-specialty/

- Domain 1: Data Engineering 20%

- Domain 2: Exploratory Data Analysis 24%

- Domain 3: Modeling 36%

- Domain 4: Machine Learning Implementation and Operations 20%

## *AWS Certified Data Analytics - Specialty*
https://aws.amazon.com/certification/certified-data-analytics-specialty/

- Domain 1: Collection 18%

- Domain 2: Storage and Data Management 22%

- Domain 3: Processing 24%

- Domain 4: Analysis and Visualization 18%

- Domain 5: Security 18%

Learning pool: [A cloud Guru](
https://learn.acloud.guru/course/312375cd-c136-4f1c-81dc-dbdcfff2d06b/dashboard)

## *advanced* coursera andrew Ng: Machine Learning Engineering for Production (MLOps) Specialization: 
https://www.coursera.org/specializations/machine-learning-engineering-for-production-mlops#courses (4 months)

1. course1: Introduction to Machine Learning in Production (https://www.coursera.org/learn/introduction-to-machine-learning-in-production?specialization=machine-learning-engineering-for-production-mlops) 3 week
2. course2: Machine Learning Data Lifecycle in Production (https://www.coursera.org/learn/machine-learning-data-lifecycle-in-production?specialization=machine-learning-engineering-for-production-mlops) 4 week
3. course3:Machine Learning Modeling Pipelines in Production (https://www.coursera.org/learn/machine-learning-modeling-pipelines-in-production?specialization=machine-learning-engineering-for-production-mlops) 5 week
4. course4: Deploying Machine Learning Models in Production (https://www.coursera.org/learn/deploying-machine-learning-models-in-production?specialization=machine-learning-engineering-for-production-mlops) 4 week

ETA 2/28 Done
## *Udacity: Data Scientist NanoDegree*

4 months
https://www.udacity.com/course/data-scientist-nanodegree--nd025

$120

70% off the regular price.
Coupon code: YLR4BRJFYDPJBL8R
Offer expires: Thursday, January 5, 2023 7:23 AM PT: https://www.udacity.com/apply/personalized-discount?status=submitted

ETA 4/30 Done

## *CFA Level1*
ETA 5/21 Done

---

![image](/stat/MIT_micromaster.png) 
## *Edx: MIT Probability - The Science of Uncertainty and Data*

Starts May 16, 2023 - Ends Sep 5, 2023

https://www.edx.org/course/probability-the-science-of-uncertainty-and-data

## *Edx: MIT Fundamentals of Statistics*

Starts May 7, 2023 - Ends Sep 7, 2023

https://www.edx.org/course/fundamentals-of-statistics


ETA 9/30 Done

## *Edx MIT Data Analysis: Statistical Modeling and Computation in Applications*

Starts Sept 2023 - Ends Dec 2023

https://www.edx.org/course/statistics-computation-and-applications

ETA 12/31 Done

## (optional) *Coursera : Machine Learning for Trading Specialization*
https://www.coursera.org/specializations/machine-learning-trading (3 months)

- Introduction to Trading, Machine Learning & GCP
- Using Machine Learning in Trading and Finance
- Reinforcement Learning for Trading Strategies
## (optional) *Web3 University*
https://www.web3.university/article/how-to-start-a-dao

## (optional) *Coursera : Columbia University: Financial Engineering and Risk Management Specialization*

https://www.coursera.org/specializations/financialengineering

- Course 1: Introduction to Financial Engineering and Risk Management (4 weeks)
- Course 2: Term-Structure and Credit Derivatives (5 weeks)
- Course 3: Optimization Methods in Asset Management (5 weeks)
- Course 4: Advanced Topics in Derivative Pricing (5 weeks)
- Course 5: Computational Methods in Pricing and Model Calibration (4 weeks)


## *Edx: MIT Capstone Exam in Statistics and Data Science*

Started Feb, 2024 to March, 2024

https://www.edx.org/course/capstone-exam-in-statistics-and-data-science

---
## Gatech OMSA 

Program Overview: 2022 Fall - 2024 Spring
#### 2022 Fall

- CSE 6242 - Data & Visual Analytics (CRN 26829) - SP, FA 

- MGT 8803 - Business Fundamentals for Analytics (CRN 26279) - SP, SU, FA

#### 2023 Spring

- ISYE 7406 - Data Mining & Statistical Learning (CRN 30734) - SP, FA 

#### 2023 Summer

- ISYE 6740 - Computational Data Analytics (CRN 27489) - SP, SU, FA 
- ISYE 6644 - Simulation (CRN 28886) - SP, SU, FA

#### 2023 Fall
  
- CS 7643 - Deep Learning (CRN 30830) - SP, SU, FA 
- CSE 6250 - Big Data Health (CRN 26283) - SP, FA 

#### 2024 Spring 

- Final Project 6 credits